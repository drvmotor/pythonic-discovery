/* This is a header file shell.h
 TITLE: This is a part of stm32desk project
 LICENSE:

 * Copyright 2016 drvmotor <drvmotor@i.ua>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 Author: (c)drvmotor
 Date:04/07/16
*/
#ifndef _SHELL_H_
#define _SHELL_H_ 1
#include <stddef.h>
extern char command_line[];
int shell(int len);

//@auto
#endif

