About to shell under stm32
Commands are used for work with perif

from stm32f4 import stm32f407 as mcu
mcu.SetClock(168*10**6)
uart = mcu.Uart(1,115200,8,'N',1)
uart.send(b'hello')
uart.recv()



<origin> <value>
<mov8> <value>
<mov16> <value>
<mov32> <value>
<and8> <mask>
<and16> <mask>
<and32> <mask>
<or8> <mask>
<or16> <mask>
<or32> <mask>

<getmem> <handle> <amount>
<purge> <handle>
<select> <handle>
<block write> <size>
<block read> <size>
<run> <handle>
--------------------------
special codes in the service table
