#! /usr/bin/python3
# configure the serial connections (the parameters differs on the device you are connecting to)
import time
import serial
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=7372800,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

PackLen = 256*4
sss=''
for n in range(PackLen):
	sss += chr(n & 0xff)
t0 = time.clock()
ser.write(sss.encode())
t1=time.clock()
x=ser.read(ser.in_waiting)
t2 = time.clock()
x=ser.read(ser.in_waiting)
t3 = time.clock()
print(len(x))
print('Interval Send={0}, Receive={1}'.format(t1-t0,t3-t1))
print('Pack spd:',PackLen/(t1-t0))
print('Diff t:',t3-t2)
ser.close()
